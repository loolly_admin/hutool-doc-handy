<p align="center">
	<a href="https://hutool.cn/"><img src="https://cdn.jsdelivr.net/gh/looly/hutool-site/images/logo.jpg" width="45%"></a>
</p>
<p align="center">
	<strong>🍬A set of tools that keep Java sweet.</strong>
</p>
<p align="center">
	👉 <a href="https://hutool.cn">https://hutool.cn/</a> 👈
</p>

<p align="center">
	<a target="_blank" href="https://search.maven.org/artifact/cn.hutool/hutool-all">
		<img src="https://img.shields.io/maven-central/v/cn.hutool/hutool-all.svg?label=Maven%20Central" />
	</a>
	<a target="_blank" href="https://license.coscl.org.cn/MulanPSL2/">
		<img src="https://img.shields.io/:license-MulanPSL2-blue.svg" />
	</a>
	<a target="_blank" href="https://www.oracle.com/java/technologies/javase/javase-jdk8-downloads.html">
		<img src="https://img.shields.io/badge/JDK-8+-green.svg" />
	</a>
	<a target="_blank" href="https://travis-ci.com/chinabugotech/hutool">
		<img src="https://travis-ci.com/chinabugotech/hutool.svg?branch=v4-master" />
	</a>
	<a href="https://www.codacy.com/gh/chinabugotech/hutool/dashboard?utm_source=github.com&amp;utm_medium=referral&amp;utm_content=chinabugotech/hutool&amp;utm_campaign=Badge_Grade">
		<img src="https://app.codacy.com/project/badge/Grade/8a6897d9de7440dd9de8804c28d2871d"/>
	</a>
	<a href="https://codecov.io/gh/chinabugotech/hutool">
		<img src="https://codecov.io/gh/chinabugotech/hutool/branch/v5-master/graph/badge.svg" />
	</a>
	<a target="_blank" href="https://gitter.im/hutool/Lobby?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge&utm_content=badge">
		<img src="https://badges.gitter.im/hutool/Lobby.svg" />
	</a>
	<a target="_blank" href='https://gitee.com/chinabugotech/hutool/stargazers'>
		<img src='https://gitee.com/chinabugotech/hutool/badge/star.svg?theme=gvp' alt='star'/>
	</a>
	<a target="_blank" href='https://github.com/chinabugotech/hutool'>
		<img src="https://img.shields.io/github/stars/chinabugotech/hutool.svg?style=social" alt="github star"/>
	</a>
</p>

-------------------------------------------------------------------------------

# Hutool主页及文档项目

## 鸣谢

文档改造来自：Handy

## 技术选型

1. 文档模板来自：[vuepress-theme-vdoing](https://doc.xugaoyi.com/)

## 文档部署

### 本地部署

```sh
# 克隆到本地
git clone git@gitee.com:chinabugotech/hutool-doc-handy.git

# 进入目录
cd hutool-doc-handy

# 安装依赖
npm install # or yarn install

# 启动服务
yarn dev
```

### 线上部署

1. 生成静态页面

```sh
./deploy.sh
```

2. 打包静态页面

```sh
cd docs/.vuepress/dist/
tar -zcvf dist.tgz *
```
将`dist.tgz`上传服务器解压即可。

```sh
tar -zxvf dist.tgz
```